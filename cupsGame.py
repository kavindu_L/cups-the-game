from tkinter import *
import random
from PIL import ImageTk, Image

root = Tk()
root.title("Cups")
root.geometry("500x350")
root.config(padx=5, pady=5)


def revealAnswer():

    for child in display_frame.winfo_children():
        child.destroy()

    for child in guess_selection_frame.winfo_children():
        child.destroy()

    Label(guess_selection_frame, text="You've selected " + str(guess.get()), font="TkCaptionFont").grid(row=0, column=0)

    cup_holder = [0, 1, 0]
    guessed_answer = guess
    random.shuffle(cup_holder)
    col = 0

    for cup in cup_holder:
        if cup == 1:
            Label(display_frame, image=photo_ball, text=str(col + 1), compound="top").grid(row=0, column=col, padx=5, pady=5)
        else:
            Label(display_frame, image=photo_no_ball, text=str(col + 1), compound="top").grid(row=0, column=col, padx=5, pady=5)

        col += 1

    if cup_holder.index(1) == (guessed_answer.get() - 1):
        Label(result_frame, text="Congratulations !!! You guessed it right", font="TkCaptionFont").grid(row=0, column=0, columnspan=3)
    else:
        Label(result_frame, text="Bad luck, you were wrong this time", font="TkCaptionFont").grid(row=0, column=0, columnspan=3)


def reset():
    guess.set(0)

    for child in result_frame.winfo_children():
        child.destroy()

    for child in guess_selection_frame.winfo_children():
        child.destroy()

    Label(guess_selection_frame, text="Take your guess").grid(row=0, column=0, sticky="W")
    Radiobutton(guess_selection_frame, variable=guess, text="1", value=1).grid(row=0, column=1, sticky="W")
    Radiobutton(guess_selection_frame, variable=guess, text="2", value=2).grid(row=0, column=2, sticky="W")
    Radiobutton(guess_selection_frame, variable=guess, text="3", value=3).grid(row=0, column=3, sticky="W")

    Label(display_frame, image=photo_cone, text="1", compound="top").grid(row=0, column=0)
    Label(display_frame, image=photo_cone, text="2", compound="top").grid(row=0, column=1)
    Label(display_frame, image=photo_cone, text="3", compound="top").grid(row=0, column=2)


photo_cone = ImageTk.PhotoImage(
    Image.open("/home/kavindul/Desktop/my_Computer/automations/tkinter/1-removebg-preview.png"))
photo_ball = ImageTk.PhotoImage(Image.open("/home/kavindul/Desktop/my_Computer/automations/tkinter/2-removebg-preview.png"))
photo_no_ball = ImageTk.PhotoImage(Image.open("/home/kavindul/Desktop/my_Computer/automations/tkinter/3-removebg-preview.png"))


display_frame = Frame(root)
display_frame.grid(row=0, column=0, padx=5, pady=5)
Label(display_frame, image=photo_cone, text="1", compound="top").grid(row=0, column=0)
Label(display_frame, image=photo_cone, text="2", compound="top").grid(row=0, column=1)
Label(display_frame, image=photo_cone, text="3", compound="top").grid(row=0, column=2)

guess_selection_frame = Frame(root)
guess_selection_frame.grid(row=1, column=0, padx=5, pady=5)
Label(guess_selection_frame, text="Take your guess").grid(row=0, column=0)
guess = IntVar()
Radiobutton(guess_selection_frame, variable=guess, text="1", value=1).grid(row=0, column=1)
Radiobutton(guess_selection_frame, variable=guess, text="2", value=2).grid(row=0, column=2)
Radiobutton(guess_selection_frame, variable=guess, text="3", value=3).grid(row=0, column=3)

buttons_frame = Frame(root)
reveal_btn = Button(buttons_frame, text="reveal", command=revealAnswer)
reset_btn = Button(buttons_frame, text="reset", command=reset)


buttons_frame.grid(row=2, column=0, columnspan=3, padx=5, pady=5)
reset_btn.grid(row=0, column=0, padx=10, pady=5)
reveal_btn.grid(row=0, column=1, padx=10, pady=5)

result_frame = Frame(root)
result_frame.grid(row=3, column=0, columnspan=3, padx=5, pady=5)

root.mainloop()
